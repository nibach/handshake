Columna handshake step1 

  Nedenstående scripts kræver følgende:
	Linux eller cygwin

  kør

  $ mkdir handshake2018.0  # git clone http://todo
  $ cd handshake2018.0/
  $ ./listleverance.sh
  $ ./hentleverance.sh 2017-11-09-CIS23.0.0.0-step1.zip   # Du har fået mail fra systematic på hvilken leverance vi skal bruge
  $ ./udpakleverance.sh 2017-11-09-CIS23.0.0.0-step1.zip

  ret i gradle.properties
  
  sseVersion=2017-11-09-CIS23.0.0.0-step1
  step1folder=2017-11-09-CIS23.0.0.0-step1
  
  åben build.gradle i notepad++ og ret i den, hvis det er nødvendigt.
  
  følgende commando kan nu publish sse jar filer til nexus
  
  $ gradle publish
  
  Nu kan man rette bookplan jar afhængighed i bookplan source kode. 
  
  
